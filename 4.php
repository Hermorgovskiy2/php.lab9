4.Вычислить расстояние между двумя точками с кооpдинатами x1,
y1, x2, y2.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите координаты точки №1 по оси Х' . PHP_EOL);
fscanf($input, '%f', $X1);
fwrite($output, 'Введите координаты точки №1 по оси Y' . PHP_EOL);
fscanf($input, '%f', $Y1);
fwrite($output, 'Введите координаты точки №2 по оси Х' . PHP_EOL);
fscanf($input, '%f', $X2);
fwrite($output, 'Введите координаты точки №2 по оси Y' . PHP_EOL);
fscanf($input, '%f', $Y2);

$distance = sqrt(($X1-$X2) ** 2 + ($Y1-$Y2) ** 2);

fprintf($output, 'Расстояние между точками равно %.2f', $distance);
