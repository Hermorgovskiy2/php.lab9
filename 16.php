16. Дано вещественное число х. Вычислить
z=sinx/(x^2 + 1) + 10y
y=ln(x^2 + 1)
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$y = log($x ** 2 + 1);
$z = sin($x) / ($x ** 2 + 1) + 10 * $y;

fprintf($output, 'Значение Z %.2f', $z);
