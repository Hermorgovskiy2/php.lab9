<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите количество секунд' . PHP_EOL);
fscanf($input, '%d', $seconds);
const SECONDS_IN_HOUR = 3600;
const SECONDS_IN_MINUT = 60;
$hours = floor($seconds / SECONDS_IN_HOUR);
$secondsWithoutHours = $seconds - $hours * SECONDS_IN_HOUR;
$minutes = round($secondsWithoutHours / SECONDS_IN_MINUT);

fprintf($output, 'Прошло %d часов, %d минут.', $hours, $minutes);
