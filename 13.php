Определить силу притяжения F между телами массы 1 2 m ,m ,
находящимися на расстоянии r друг от друга.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите массу тела №1' . PHP_EOL);
fscanf($input, '%d', $m1);
fwrite($output, 'Введите массу тела №2' . PHP_EOL);
fscanf($input, '%d', $m2);
fwrite($output, 'Введите расстояние между объектами 1 и 2' . PHP_EOL);
fscanf($input, '%d', $r);

const GRAVITATIONAL_CONSTANT = 6.67 * 10 ** -11;
$force = round(GRAVITATIONAL_CONSTANT * $m1 * $m2 / $r ** 2, 9);

fprintf($output, 'Сила приятжения между объектам равна %.9f', $force);