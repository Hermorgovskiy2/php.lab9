16. Даны целые числа m и n (0 < m <= 12, 0 <= n < 60),
указывающие момент времени: “m часов, n минут”. Определить, сколько минут
должно пройти до того момента, когда часовая и минутная стрелки на
циферблате будут показывать 8 часов, 20 минут.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите количество часов (до 12-ти включительно)' . PHP_EOL);
fscanf($input, '%d', $hours);
fwrite($output, 'Введите количество минут (от 0 до 60-ти)' . PHP_EOL);
fscanf($input, '%d', $minutes);

const EIGHT_HOURS = '8';
const MINUTES_IN_HOUR = '60';
const TWELVE_HOURS = '12';
$cutoff = EIGHT_HOURS * MINUTES_IN_HOUR + 20;
$setTime = $hours * MINUTES_IN_HOUR + $minutes;
if ($setTime > $cutoff) {
    $minutesToCutoff = MINUTES_IN_HOUR * TWELVE_HOURS - ($setTime - $cutoff);
    fprintf($output, 'До 8 часов 20 минут осталось %d минут.', $minutesToCutoff);
}   else {
    $minutesToCutoff = $cutoff - $setTime;
    fprintf($output, 'До 8 часов 20 минут осталось %d минут.', $minutesToCutoff);
}