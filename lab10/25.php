25. Дано: x и y  пpоизвольные вещественные числа. Опpеделить z
по следующей фоpмуле: z = { max(x,y), если у<0, min(x,y), если у>=0
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Задайте вещественное число X' . PHP_EOL);
fscanf($input, '%f', $x);
fwrite($output, 'Задайте вещественное число Y' . PHP_EOL);
fscanf($input, '%f', $y);

if ($y < 0) {
    $z = max($x, $y);
    fprintf($output, 'Z равен %.1f', $z);
}   else {
    $z = min($x, $y);
    fprintf($output, 'Z равен %.1f', $z);
}
