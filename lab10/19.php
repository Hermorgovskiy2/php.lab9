19. Определить, принадлежит ли точка с координатами (x,y) кругу
радиуса r с центром в начале координат.
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Задайте координаты точки по оси Х' . PHP_EOL);
fscanf($input, '%f', $x1);
fwrite($output, 'Задайте координаты точки по оси Y' . PHP_EOL);
fscanf($input, '%f', $y1);
fwrite($output, 'Задайте радиус круга' . PHP_EOL);
fscanf($input, '%f', $radius);

if ($radius ** 2 == $x1 ** 2 + $y1 ** 2) {
    fprintf($output, 'Точка с координатами (%.1f,%.1f) принадлежит окружности с радиусом %.1f', $x1, $y1, $radius);
} else {
    fprintf($output, 'Точка с координатами (%.1f,%.1f) не принадлежит окружности с радиусом %.1f', $x1, $y1, $radius);
}
