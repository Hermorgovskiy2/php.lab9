24. Дано вещественное x. Составить пpогpамму вычисления
y=f(x), если функция f(x) задана графически на рис. 2.17.
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Задайте вещественное число X' . PHP_EOL);
fscanf($input, '%f', $x);

if ($x <= 0) {
    $y = - $x;
    fprintf($output, 'Y равен %.1f', $y);
}
if ($x > 0 && $x <= 1 ) {
    $y = $x;
    fprintf($output, 'Y равен %.1f', $y);
}
if ($x > 1 && $x <= 3 ) {
    $y = 1;
    fprintf($output, 'Y равен %.1f', $y);
}
if ($x > 3 ) {
    $y = 4 - $x;
    fprintf($output, 'Y равен %.1f', $y);
}