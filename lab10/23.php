23. Дано вещественное x. Составить пpогpамму вычисления
y=f(x), если функция f(x) задана графически на рис. 2.16.
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Задайте вещественное число X' . PHP_EOL);
fscanf($input, '%f', $x);

if ($x <= 0) {
    $y = - $x;
    fprintf($output, 'Y равен %.1f', $y);
}
if ($x > 0) {
    $y = $x ** 2;
    fprintf($output, 'Y равен %.1f', $y);
}

