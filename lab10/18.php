18. Дано: x и y  пpоизвольные вещественные числа. Опpеделить z
по следующей фоpмуле:
z = max(x,y), если х < 0, min(x,y), если х =>0
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите число Х.' . PHP_EOL);
fscanf($input, '%f', $x);
fwrite($output, 'Введите число Y.' . PHP_EOL);
fscanf($input, '%f', $y);
$first = max($x, $y);
$second = min($x, $y);

if ($x < 0) {
    fprintf($output, 'Значение переменной Z равно %.2f', $first);
}   else {
    fprintf($output, 'Значение переменной Z равно %.2f', $second);
}