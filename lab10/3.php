Дано вещественное x. Вычислить z=f(x)
3. f(x)=e^(a/x) + (x/cosax)^1/2, a=0.25
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите число Х' . PHP_EOL);
fscanf($input, '%f', $x);

$a = 0.25;
const NUMBER_OF_ELLER = 2.7182818284;
try {
    $fFromX = NUMBER_OF_ELLER ** ($a/$x) + sqrt($x / cos($a * $x));
}   catch (Throwable $exception) {
    $fFromX = false;
}
if ($fFromX == false || is_nan($fFromX)) {
    $z = $fFromX;
    fprintf($output, 'С таким значением X как %.1f вычислить Z невозможно. Попробуй еще раз с другим значеним X', $x);
}   else {
    $fFromX = NUMBER_OF_ELLER ** ($a/$x) + sqrt($x / cos($a * $x));
    fprintf($output, 'Значение Z равно %.f', $fFromX);
}
