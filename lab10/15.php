15. Даны целые числа m и n (0 < m <= 12, 0 <= n < 60),
указывающие момент времени: “m часов, n минут”. Определить, сколько
минут должно пройти до того момента, когда наступит ровно 12 часов.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите количество часов (до 12-ти включительно)' . PHP_EOL);
fscanf($input, '%d', $hours);
fwrite($output, 'Введите количество минут (от 0 до 60-ти)' . PHP_EOL);
fscanf($input, '%d', $minutes);

const TWELVE_HOURS = '12';
const MINUTES_IN_HOUR = '60';
$countHours = TWELVE_HOURS - $hours;
$fromHoursToMinutes = $countHours * MINUTES_IN_HOUR;
$countMinutes = $fromHoursToMinutes - $minutes;
if ($countHours == 0) {
    fprintf($output, '12 часов уже наступило');
} else {
    fprintf($output, 'До 12-ти часов осталось %d минут.', $countMinutes);
}