Дано вещественное x. Вычислить z=f(x)
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите число Х' . PHP_EOL);
fscanf($input, '%f', $x);

try {
    $fFromX = log(sin($x)) + sqrt(sin($x) / $x);
}   catch (Throwable $exception) {
    $fFromX = false;
}

if ($fFromX == false || is_nan($fFromX)) {
    $z = $fFromX;
    fprintf($output, 'С таким значением X как %.1f вычислить Z невозможно. Попробуй еще раз с другим значеним X', $x);
}   else {
    $fFromX = log(sin($x)) + sqrt(sin($x) / $x);
    fprintf($output, 'Значение Z равно %.f', $fFromX);
}