20. Написать программу определения суммы большего и меньшего
из трех заданных чисел.
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите первое трехзначное число' . PHP_EOL);
fscanf($input, '%d', $first);
fwrite($output, 'Введите второе трехзначное число' . PHP_EOL);
fscanf($input, '%d', $second);
fwrite($output, 'Введите третье трехзначное число' . PHP_EOL);
fscanf($input, '%d', $third);

$maximum = max($first, $second, $third);
$minimum = min($first, $second, $third);
$sum = $maximum + $minimum;
fprintf($output, 'Сумма минимального числа %d и максимального %d равна %d', $minimum, $maximum, $sum);
