22. Даны три точки, заданные своим координатами, определить
принадлежат ли эти точки одной прямой.
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите коорднаты точки А по оси Х' . PHP_EOL);
fscanf($input, '%f', $x1);
fwrite($output, 'Введите коорднаты точки А по оси Y' . PHP_EOL);
fscanf($input, '%f', $y1);
fwrite($output, 'Введите коорднаты точки B по оси Х' . PHP_EOL);
fscanf($input, '%f', $x2);
fwrite($output, 'Введите коорднаты точки B по оси Y' . PHP_EOL);
fscanf($input, '%f', $y2);
fwrite($output, 'Введите коорднаты точки C по оси X' . PHP_EOL);
fscanf($input, '%f', $x3);
fwrite($output, 'Введите коорднаты точки C по оси Y' . PHP_EOL);
fscanf($input, '%f', $y3);

if (($x1 - $x3) * ($y2 - $y3) == ($x2-$x3) * ($y1 - $y3)) {
    fprintf($output, 'Точки А (%.1f,%.1f), точка B (%.1f,%.1f), точка C (%.1f,%.1f) принадлежат одной прямой', $x1, $y1, $x2, $y2, $x3, $y3);
}   else {
    fprintf($output, 'Точки А (%.1f,%.1f), точка B (%.1f,%.1f), точка C (%.1f,%.1f) не принадлежат одной прямой', $x1, $y1, $x2, $y2, $x3, $y3);
}