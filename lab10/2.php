Дано вещественное x. Вычислить z=f(x)
2. f(x) = (ax^1/2 / lnx) - (cosx^2/x^2)^1/2, a=10.
<?php
$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите число Х' . PHP_EOL);
fscanf($input, '%f', $x);

$a = 10;
try {
    $fFromX = (sqrt($a * $x) / log($x)) - sqrt(cos($x ** 2) / $x ** 2);
}   catch (Throwable $exception) {
    $fFromX = false;
}
if ($fFromX == false || is_nan($fFromX)) {
    $z = $fFromX;
    fprintf($output, 'С таким значением X как %.1f вычислить Z невозможно. Попробуй еще раз с другим значеним X', $x);
}   else {
    $fFromX = (sqrt($a * $x) / log($x)) - sqrt(cos($x ** 2) / $x ** 2);
    fprintf($output, 'Значение Z равно %.f', $fFromX);
}