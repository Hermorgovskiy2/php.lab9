12. Три резистора 1 2 3 R , R , R соединены параллельно. Найти
сопротивление соединения.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите сопротивление первого резистора' . PHP_EOL);
fscanf($input, '%d', $R1);
fwrite($output, 'Введите сопротивление второго резистора' . PHP_EOL);
fscanf($input, '%d', $R2);
fwrite($output, 'Введите сопротивление третьего резистора' . PHP_EOL);
fscanf($input, '%d', $R3);

$allR = 1 / (1 / $R1 + 1 / $R2 + 1 / $R3);

fprintf($output, 'Сопротивление сети равно %.2f Ом', $allR);