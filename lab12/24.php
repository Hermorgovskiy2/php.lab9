<?php
//24. Вычислить математическое ожидание М и дисперсию D. Где
//"что-то на математическом"
require_once 'functions.php';
//$array = [1, 2, 3];
$array = [-22, 0, 2, 0, -2, 22, -4, 0, 4, -4, 4, -1, 15, 10, 22, 15, 22];
$mathExpectation = getMathExpectation($array);
$dispersion = getDispersion($array, $mathExpectation);
echo 'Дисперсия ровна ' . $dispersion . ', мат ожидания равны ' . $mathExpectation;
