<?php
//21. Определить количество чисел в наиболее длинной
//последовательности из подряд идущих нулей.
require_once 'functions.php';
$arrayZero = [0, 0, 1, 0, 0, 4, 6, 22, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0];
$longestChainOfZero = getLongestChainOfZero($arrayZero);
echo $longestChainOfZero;