<?php
//14. Вычислить сумму отрицательных, произведение положительных и
//количество нулевых элементов массива с нечетными индексами.
require_once 'functions.php';
$array = [22, 0, 2, 0, -2, 0, -4, 0, 4, -4, 4, -1, 10, 15, 16, 15];
$newArray = getOddIndex($array);
var_dump($newArray);
$sumNegativeElements = getSumNegativeElements($newArray);
$productPositiveElements = getProductPositiveElements($newArray);
$countZeroElements = getCountZeroElements($newArray);
echo 'Сумма негативных элементов ' . $sumNegativeElements . PHP_EOL . 'произведение позитивных элементов ' . $productPositiveElements . PHP_EOL . 'Количество нулевых элементов ' . $countZeroElements;