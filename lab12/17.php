<?php
//17. Заменить все наибольшие элементы массива значением
//минимального элемента.
require_once 'functions.php';
$array = [22, 0, 2, 0, -2, 0, -4, 0, 4, -4, 4, -1, 10, 15, 22, 15];
$newArray = changeMaxElementsOnMinElement($array);
var_dump($newArray);