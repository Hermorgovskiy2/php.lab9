<?php
//20. Определить порядковый номер того элемента массива, который
//наиболее близко к некоторому целому числу х (х, как и массив, вводится с
//клавиатуры).
require_once 'functions.php';
$x = 10;
$array = [-22, 0, 2, 0, -2, 0, -4, 0, 4, -4, 4, -1, 15, 10, 22, 15, 0];
$nearest = getNearestElement($array, $x);
echo $nearest;