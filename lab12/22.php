<?php
//22. Определить, сколько элементов массива принимает наибольшее значение.
require_once 'functions.php';
$array = [-22, 0, 2, 0, -2, 22, -4, 0, 4, -4, 4, -1, 15, 10, 22, 15, 22];
$numberOfHighestValue = getNumberOfHighestValue($array);
echo $numberOfHighestValue;