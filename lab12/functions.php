<?php

function arrayCount(array $array)
{
    $count = 0;
    foreach ($array as $value) {
        $count++;
    }
    return $count;
}

function getIndexMax(array $array): int
{
    $maxIndex = 0;
    foreach ($array as $key => $value) {
        if ($value > $array[$maxIndex]) {
            $maxIndex = $key;
        }
    }
    return $maxIndex;
}

function getAvgPositive(array $array): float
{
    $sum = 0;
    $count = 0;
    foreach ($array as $key => $value) {
        if ($value > 0) {
            $sum += $value;
            $count++;
        }
    }
    return $sum / $count;
}

function getPositiveElements(array $arrayA): array
{
    $arrayB = [];
    foreach ($arrayA as $value) {
        if ($value > 0) {
            $arrayB[] = $value;
        }
    }
    return $arrayB;
}

function getIndexMin(array $array): int
{
    $minIndex = 0;
    foreach ($array as $key => $value) {
        if ($value < $array[$minIndex]) {
            $minIndex = $key;
        }
    }
    return $minIndex;
}

function getSumPositiveElement(array $array): int
{
    $sum = 0;
    foreach ($array as $value) {
        if ($value > 0) {
            $sum += $value;
        }
    }
    return $sum;
}

function getNumberPositiveElements(array $arrayA): array
{
    $arrayB = [];
    foreach ($arrayA as $key => $value) {
        if ($value > 0) {
            $arrayB[] = $key;
        }
    }
    return $arrayB;
}

function getNewArrayB(array $arrayA): array
{
    $arrayB = [];
    $sum = 0;
    foreach ($arrayA as $key => $value) {
        $sum = $sum + $arrayA[$key];
        $arrayB[] = $sum;
    }
    return $arrayB;
}

function getFinalIndex(array $array): int
{
    $count = arrayCount($array);
    $finalIndex = $count - 1;
    return $finalIndex;
}

function getNewArrayBNotRepeatElements(array $arrayA): array
{
    $count = arrayCount($arrayA);
    $arrayB = [];
    for ($i = 0; $i < $count; $i++) {
        foreach ($arrayA as $key => $value) {
            if ($arrayA[$i] === $arrayA[$key] && $key > $i) {
                $arrayB[] = $arrayA[$key];
            }
        }
    }
    $countArrayB = arrayCount($arrayB);
    for ($i = 0; $i < $countArrayB; $i++)
        foreach ($arrayA as $keyA => $valueA) {
            if ($arrayA[$keyA] === $arrayB[$i]) {
                unset($arrayA[$keyA]);
            }
        }
    foreach ($arrayA as $valueA) {
        $arrayB[] = $valueA;
    }
    return $arrayB;
}

function getElementsFromMoreOneHundredThenOthers(array $arrayA): array
{
    $arrayB = [];
    $arrayC = [];
    foreach ($arrayA as $value) {
        if ($value > 100) {
            $arrayB[] = $value;
        } else {
            $arrayC[] = $value;
        }
    }
    foreach ($arrayC as $value) {
        $arrayB[] = $value;
    }
    return $arrayB;
}

function getNumberOfPositiveElementsBeforeNegative(array $array): int
{
    $count = 0;
    foreach ($array as $value) {
        if ($value > 0) {
            $count++;
        } else {
            break;
        }
    }
    return $count;
}

function changeZeroElementsOnOneBeforeNegativeElements(array $array): array
{
    foreach ($array as $key => $value) {
        if ($value >= 0) {
            if ($value === 0) {
                $array[$key] = 1;
            }
        } else {
            break;
        }
    }
    return $array;
}

function getOddIndex(array $array): array
{
    $newArray = [];
    foreach ($array as $key => $value) {
        if ($key % 2 != 0) {
            $newArray[] = $value;
        }
    }
    return $newArray;
}

function getSumNegativeElements(array $newArray): int
{
    $sum = 0;
    foreach ($newArray as $value) {
        if ($value < 0) {
            $sum += $value;
        }
    }
    return $sum;
}

function getProductPositiveElements(array $newArray): int
{
    $product = 1;
    foreach ($newArray as $value) {
        if ($value > 0) {
            $product *= $value;
        }
    }
    return $product;
}

function getCountZeroElements(array $newArray): int
{
    $count = 0;
    foreach ($newArray as $value) {
        if ($value === 0) {
            $count++;
        }
    }
    return $count;
}

function getPositiveElementsThenOthers(array $arrayA): array
{
    $arrayB = [];
    $arrayC = [];
    foreach ($arrayA as $value) {
        if ($value > 0) {
            $arrayB[] = $value;
        } else {
            $arrayC[] = $value;
        }
    }
    foreach ($arrayC as $value) {
        $arrayB[] = $value;
    }
    return $arrayB;
}

function getEvenIndexThenOdd(array $arrayA): array
{
    $arrayB = [];
    $arrayC = [];
    foreach ($arrayA as $key => $value) {
        if ($key % 2 == 0) {
            $arrayB[] = $value;
        } else {
            $arrayC[] = $value;
        }
    }
    foreach ($arrayC as $value) {
        $arrayB[] = $value;
    }
    return $arrayB;
}

function changeMaxElementsOnMinElement(array $array): array
{
    $indexMin = getIndexMin($array);
    $minElement = $array[$indexMin];
    $indexMax = getIndexMax($array);
    $maxElement = $array[$indexMax];
    foreach ($array as $key => $value) {
        if ($value === $maxElement) {
            $array[$key] = $minElement;
        }
    }
    return $array;
}

function getNewArrayFrequencyOfCoincidenceOfValues(array $arrayA, $arrayB): array
{
    foreach ($arrayB as $keyB => $valueB) {
        $count = 0;
        foreach ($arrayA as $keyA => $valueA) {
            if ($valueB === $valueA) {
                $count++;
                $arrayX[$valueB] = $count;
            }
        }
    }
    return $arrayX;
}

function changeZeroOnSumOfMultiples(array $array, $x): array
{
    $sum = 0;
    foreach ($array as $key => $value) {
        if ($value % $x == 0) {
            $sum += $value;
        }
    }
    foreach ($array as $key => $value) {
        if ($value === 0) {
            $array[$key] = $sum;
        }
    }
    return $array;
}

function getNearestElement(array $array, $x): int
{
    $newArray = [];
    foreach ($array as $value) {
        $difference = abs($value - $x);
        $newArray[] = $difference;
    }
    $minIndex = getIndexMin($newArray);
    return $minIndex;
}

function sorting(array $array, $x): array
{
    $array[] = $x;
    $count = arrayCount($array);
    $iterations = $count - 1;
    for ($i = 0; $i < $count; $i++) {
        $changes = false;
        for ($key = 0; $key < $iterations; $key++) {
            if ($array[$key] > $array[$key + 1]) {
                $changes = true;
                list($array[$key], $array[$key + 1]) = array($array[$key + 1], $array[$key]);
            }
        }
        $iterations--;
        if (!$changes) {
            return $array;
        }
    }
    return $array;
}

function getLongestChainOfZero(array $arrayZero): int
{
    $zeroCounts = [];
    $countZero = 0;
    foreach ($arrayZero as $key => $value) {
        if ($value === 0) {
            $countZero++;
            $zeroCounts[] = $countZero;
        } else {
            $countZero = 0;
        }
    }
    $maxIndex = getIndexMax($zeroCounts);
    $maxElement = $zeroCounts[$maxIndex];
    return $maxElement;
}

function getNumberOfHighestValue(array $array): int
{
    $maximumIndex = 0;
    foreach ($array as $key => $value) {
        if ($value > $array[$maximumIndex]) {
            $maximumIndex = $key;
        }
    }
    $maximumElement = $array[$maximumIndex];
    $count = 0;
    foreach ($array as $value) {
        if ($value === $maximumElement) {
            $count++;
        }
    }
    return $count;
}

function getCircleWithAllPoints(array $arrayA, $arrayB): float
{
    $count = arrayCount($arrayA);
    $newArray = [];
    for ($i = 0; $i < $count; $i++) {
        $newArray[$arrayA[$i]] = $arrayB[$i];
    }
    foreach ($newArray as $key => $value) {
        $radius[] = $key ** 2 + $value ** 2;
    }
    $maxIndex = getIndexMax($radius);
    $radiusOfCircle = sqrt($radius[$maxIndex]);
    return round($radiusOfCircle, 2);
}

function getThreeHighestElements(array $array): array
{
    $highestElements = [];
    for ($i = 0; $i < 3; $i++) {
        $maxIndex = getIndexMax($array);
        $highestElements[$i] = $maxIndex;
        unset($array[$maxIndex]);
    }
    return $highestElements;
}

function getNewArrayAccordingToFormula(array $arrayA): array
{
    $count = arrayCount($arrayA);
    $sum = 0;
    $arrayB = [];
    foreach ($arrayA as $key => $value) {
        $sum = $sum + $arrayA[$key];
        $count--;
        $arrayB[$count] = $sum;
    }
    return $arrayB;
}

function getMathExpectation(array $array): float
{
    $count = 0;
    $sum = 0;
    foreach ($array as $key => $value) {
        $count++;
        $sum += $value;
    }
    $mathExpectation = $sum / $count;
    return round($mathExpectation, 2);
}

function getDispersion(array $array, $mathExpectation): float
{
    $count = 0;
    $sum = 0;
    foreach ($array as $key => $value) {
        $count++;
        $sum += ($value - $mathExpectation) ** 2;
    }
    $dispersion = sqrt($sum / ($count - 1));
    return round($dispersion, 2);
}