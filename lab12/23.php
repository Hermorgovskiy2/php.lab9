<?php
//23. Кроме массива А, в этой задаче дан одномерный массив В.
//Рассматривая пары чисел i a и i b как координаты точек на плоскости,
//определить радиус наименьшего круга (с центром в начале координат),
//внутрь которого попадают все эти точки.
require_once 'functions.php';
$arrayA = [10, 12, 13, -2];
$arrayB = [10, 1, -7, 0];
$radiusOfCircle = getCircleWithAllPoints($arrayA, $arrayB);
echo $radiusOfCircle;
