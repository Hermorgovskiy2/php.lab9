<?php
//6. Поменять местами минимальный и максимальный элементы массива.
require_once 'functions.php';

$array = [1, 3, 2, -2, 0, 10];
$minIndex = getIndexMin($array);
$maxIndex = getIndexMax($array);
$maxElement = $array[$maxIndex];
$minElement = $array[$minIndex];
$array[$maxIndex] = $minElement;
$array[$minIndex] = $maxElement;
var_dump($array);