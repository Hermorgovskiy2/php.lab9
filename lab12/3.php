<?php
//3. Заменить минимальный элемент массива суммой положительных
//элементов массива.
require_once 'functions.php';

$array = [-2, 1 , -3, 21 , 0];
$minIndex = getIndexMin($array);
$sumPositive = getSumPositiveElement($array);
$array[$minIndex] = $sumPositive;
var_dump($array);