<?php
//7. Поменять местами минимальный и первый элементы массива.
require_once 'functions.php';

$array = [1, 3, 2, -2, 0, 10];
$minIndex = getIndexMin($array);
$minElement = $array[$minIndex];
$firstElement = $array[0];
$array[0] = $minElement;
$array[$minIndex] = $firstElement;
var_dump($array);