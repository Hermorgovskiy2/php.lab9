<?php
//8. Поменять местами минимальный и последний элементы массива.
require_once 'functions.php';

$array = [1, 3, 2, 0, 10, 9, 13, -3, -2];
$minIndex = getIndexMin($array);
$finalIndex = getFinalIndex($array);
if ($minIndex === $finalIndex) {
    var_dump($array);
}   else {
    $finalElement = $array[$finalIndex];
    $minElement = $array[$minIndex];
    $array[$finalIndex] = $minElement;
    $array[$minIndex] = $finalElement;
    var_dump($array);
}
