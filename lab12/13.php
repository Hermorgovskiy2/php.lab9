<?php
//13. Заменить все нулевые элементы, предшествующие первому
//отрицательному, единицей.
require_once 'functions.php';
$array = [22, 0, 2, 0, -2, 0, -4, 0, 4, -4];
$newArray = changeZeroElementsOnOneBeforeNegativeElements($array);
var_dump($newArray);