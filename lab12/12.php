<?php
//12. Определить количество положительных элементов массива,
//предшествующих первому отрицательному.
require_once 'functions.php';
$array = [22, 13, 4, -3, -2, 1];
$numberOfPositiveElementsBeforeNegative = getNumberOfPositiveElementsBeforeNegative($array);
echo $numberOfPositiveElementsBeforeNegative;