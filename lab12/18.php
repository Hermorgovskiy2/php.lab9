<?php
//18. Кроме массива А, в этой задаче дан массив В. Сформировать массив
//Х, элементы которого равны частоте встречаемости элементов массива В
//среди элементов массива А.
require_once 'functions.php';
$arrayA = [22, 0, 2, 0, -2, 0, -4, 0, 4, -4, 4, -1, 10, 15, 22, 15, 0];
$arrayB = [0, 1, 2, 3, 4, 5, 0];
if (arrayCount($arrayA) !== 0 && arrayCount($arrayB) !== 0) {
    $arrayX = getNewArrayFrequencyOfCoincidenceOfValues($arrayA, $arrayB);
    var_dump($arrayX);
} else {
    echo 'Невозможно выполнить условия задачи - один или оба массива пусты.';
}
