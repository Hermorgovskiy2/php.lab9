24. Дано вещественное число х. Вычислить
y=e^-x + ln(x^2 + 4)^1/2
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$e = 2.718281828459;
$y = $e ** -$x + log(sqrt($x ** 2 + 4));

fprintf($output, 'Значение y %.2f', $y);
