23. Дано вещественное число х. Вычислить
z=(cosx/sin^2(x+1)+5*10^-1) - 0.0012
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$z = cos($x) / (pow(sin($x + 2), 2) + 5 * 10 ** -1) - 0.0012;

fprintf($output, 'Значение z %.2f', $z);

