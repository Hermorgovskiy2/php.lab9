24. Дано вещественное число х. Вычислить
z=cos(x^4 + 2)^3 + (x^4 + )^1/2
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$z = cos($x ** 4 + 2) ** 3 + sqrt($x ** 4 + 2);

fprintf($output, 'Значение z %.2f', $z);