6.Целой переменной h присвоить третью от конца цифру в записи целого
положительного числа k (k больше 100. Например, если k=130985, то h=9).
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Задайте целое число более ста' . PHP_EOL);
fscanf($input, "%d", $number);
$firstStep = floor($number / 100);
$secondStep = floor($number / 1000) * 10;
$result = $firstStep - $secondStep;

fprintf($output, 'Третья цифра с конца в заданном числе %d это %d', $number, $result);