20. Дано вещественное число х. Вычислить
y=sinx+(x^2 + 1)^5
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$y = sin($x) + ($x ** 2 + 1) ** 5;

fprintf($output, 'Значение y %.2f', $y);
