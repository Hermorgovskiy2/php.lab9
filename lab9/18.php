18. Дано вещественное число х. Вычислить
y=ln((x^2+4)^1/2) + 14.48*10^-5
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$y = log(sqrt($x ** 2 + 4)) + 14.48 * 10 ** -5;

fprintf($output, 'Значение y %.2f', $y);
