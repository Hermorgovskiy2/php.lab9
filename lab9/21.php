21. Дано вещественное число х. Вычислить
y=cos(2x^2+5)+(2x^2 + 5)^1/2
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$y = cos(2 * $x ** 2 + 5) + sqrt(2 * $x ** 2 + 5);

fprintf($output, 'Значение y %.2f', $y);
