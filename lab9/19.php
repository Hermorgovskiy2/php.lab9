19. Дано вещественное число х. Вычислить
z=(x^4 + 2)^1/2 - 0.024*10^2
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$z = sqrt($x ** 4 + 4) - 0.024 * 10 ** 2;

fprintf($output, 'Значение z %.2f', $z);