8.Целой переменной s присвоить сумму цифр трехзначного целого числа k.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите трехзначное целое число' . PHP_EOL);
fscanf($input, '%d', $number);

$first = floor($number / 100);
$second = floor($number / 10) - $first * 10;
$third = $number - (floor($number / 10) * 10);
$sum = $first + $second + $third;

fprintf($output, 'Сумма цифр числа %d равна %d', $number, $sum);