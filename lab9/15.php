15. Треугольник задан координатами своих вершин. Найти площадь
треугольника. (тут периметр, в задании №5 - площадь)
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Задайте координаты вершины А: X1 равен' . PHP_EOL);
fscanf($input, '%f', $X1);
fwrite($output, 'Задайте координаты вершины А: Y1 равен' . PHP_EOL);
fscanf($input, '%f', $Y1);
fwrite($output, 'Задайте координаты вершины B: X2 равен' . PHP_EOL);
fscanf($input, '%f', $X2);
fwrite($output, 'Задайте координаты вершины B: Y2 равен' . PHP_EOL);
fscanf($input, '%f', $Y2);
fwrite($output, 'Задайте координаты вершины C: X3 равен' . PHP_EOL);
fscanf($input, '%f', $X3);
fwrite($output, 'Задайте координаты вершины C: Y3 равен' . PHP_EOL);
fscanf($input, '%f', $Y3);

$lengthAB = sqrt(($X2 - $X1) ** 2 + ($Y2 - $Y1) ** 2);
$lengthAC = sqrt(($X3 - $X1) ** 2 + ($Y3 - $Y1) ** 2);
$lengthBC = sqrt(($X3 - $X2) ** 2 + ($Y3 - $Y2) ** 2);
$perimeter = $lengthAB + $lengthAC + $lengthBC;

fprintf($output, 'Периметр треугольника равен %.2f см', $perimeter);