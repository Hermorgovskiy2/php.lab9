22. Дано вещественное число х. Вычислить
y=cos^2*x+(x^2 + 1)^1/2 + 1.15
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$y = pow(cos($x), 2) + sqrt($x ** 2 +1) + 1.15;

fprintf($output, 'Значение y %.2f', $y);
