9.Определить f – угол (в градусах) между положением часовой стрелки
в начале суток и её положением в h часов, m минут и s секунд
(0 <= h <=11,0 <= m,s <= 59 ).
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите часы (от 0 до 11)' . PHP_EOL);
fscanf($input, '%d', $hour);
fwrite($output, 'Введите минуты (от 0 до 59)' . PHP_EOL);
fscanf($input, '%d', $minut);
fwrite($output, 'Введите секунды (от 0 до 59)' . PHP_EOL);
fscanf($input, '%d', $second);

const DEGREES_IN_ONE_HOUR = 30;
const DEGREES_IN_ONE_MINUT = 0.5;
const DEGREES_IN_ONE_SECOND = 0.0083;
$arrow = $hour * DEGREES_IN_ONE_HOUR + $minut * DEGREES_IN_ONE_MINUT + $second * DEGREES_IN_ONE_SECOND;


fprintf($output, 'Угол между стрелкой начала суток и стрелкой %d:%d:%d равен %.2f градусов', $hour, $minut, $second, $arrow);