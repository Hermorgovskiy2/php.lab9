14. Определить время, через которое встретятся два тела, движущиеся
навстречу друг другу с постоянной скоростью, если известны их скорости и
начальное расстояние между ними.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите скорость объекта №1' . PHP_EOL);
fscanf($input, '%d', $speedOne);
fwrite($output, 'Введите скорость объекта №2' . PHP_EOL);
fscanf($input, '%d', $speedTwo);
fwrite($output, 'Введите расстояние между ними' . PHP_EOL);
fscanf($input, '%d', $distance);

const MINUTS_IN_HOUR = 60;
$time = $distance / ($speedOne + $speedTwo) * MINUTS_IN_HOUR;

fprintf($output, 'Время, через которое встретятся два объекта составит %d минут', $time);