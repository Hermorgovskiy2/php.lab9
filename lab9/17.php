17. Дано вещественное число х. Вычислить
y=(sinx)^2 + ln(x^2+1)
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите значение Х' . PHP_EOL);
fscanf($input, '%f', $x);

$y = sqrt(sin($x)) + log(sqrt($x) +1);

fprintf($output, 'Значение y %.2f', $y);
