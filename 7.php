7.Присвоить целой переменной d первую цифру из дробной части
положительно вещественного числа x (так, если х=32.597, то d=5).
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите дробное положительное число' . PHP_EOL);
fscanf($input, '%f', $number);
$firstStep = floor($number * 10);
$secondStep = floor($number) * 10;
$d = $firstStep - $secondStep;

fprintf($output, 'Первая цифра из дробной части %f это %d', $number, $d);