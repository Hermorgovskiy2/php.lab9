10. Определить h – полное количество часов и m полное количество
минут, прошедших от начала суток до того момента (в первой половине дня),
когда часовая стрелка повернулась на f градусов (0  f  360, f –
вещественное число).
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Введите число градусов (от 0 до 360)' . PHP_EOL);
fscanf($input, '%d', $degrees);

const DEGREES_IN_ONE_HOUR = 30;

$hours = floor($degrees / DEGREES_IN_ONE_HOUR);
$minute = ($degrees % DEGREES_IN_ONE_HOUR) * 2;

fprintf($output, 'Полное количество часов %d, полное количество минут %d', $hours, $minute);