5.Тpеугольник задан координатами своих веpшин. Hайти пеpиметp
тpеугольника.
<?php

$input = fopen('php://stdin', 'r');
$output = fopen('php://stdout', 'w');

fwrite($output, 'Задайте координаты вершины А: X1 равен' . PHP_EOL);
fscanf($input, '%f', $X1);
fwrite($output, 'Задайте координаты вершины А: Y1 равен' . PHP_EOL);
fscanf($input, '%f', $Y1);
fwrite($output, 'Задайте координаты вершины B: X2 равен' . PHP_EOL);
fscanf($input, '%f', $X2);
fwrite($output, 'Задайте координаты вершины B: Y2 равен' . PHP_EOL);
fscanf($input, '%f', $Y2);
fwrite($output, 'Задайте координаты вершины C: X3 равен' . PHP_EOL);
fscanf($input, '%f', $X3);
fwrite($output, 'Задайте координаты вершины C: Y3 равен' . PHP_EOL);
fscanf($input, '%f', $Y3);

$modulX1 = $X1 - $X3;
$modulX2 = $Y1 - $Y3;
$modulY1 = $X2 - $X3;
$modulY2 = $Y2 - $Y3;

$modul = $modulX1 * $modulY2 - $modulY1 * $modulX2;

$S = abs(0.5*$modul);

fprintf($output, 'Площадь треугольника равна %.2f', $S);

